(function ($, Drupal) {
    Drupal.behaviors.hjeeplBehavior = {
        attach: function (context, settings) {
			// Top margin as per the header height.
			var headerHeight = $('.site-header').outerHeight();
			$('main').css('margin-top', headerHeight);

			$(window).scroll(function () {
				if ($(window).scrollTop() > headerHeight) {
					$('.site-header').addClass("sticky");
				} else{
					$('.site-header').removeClass("sticky");
				}
			});

            $('.navbar-toggler').click(function() {
              $('.region-primary-menu').toggleClass('show');
              $(this).toggleClass('active');
            });

            // Project listing page
            if($('.paragraph--type--project-listing-section')) {
                $('.paragraph--type--project-listing-section').each(function(){
                    // Cache the highest
                    var highestBox = 0;
                    // Select and loop the elements you want to equalise
                    $('.our-services__items h4', this).each(function(){
                      // If this box is higher than the cached highest then store it
                      if($(this).height() > highestBox) {
                        highestBox = $(this).height();
                      }
                    });
                    // Set the height of all those children to whichever was highest
                    $('.our-services__items h4',this).height(highestBox);
               });
            }

            // Client page
            if($('.paragraph--type--client-listing')) {
                $('.paragraph--type--client-listing').each(function(){
                    // Cache the highest
                    var highestBox = 0;
                    // Select and loop the elements you want to equalise
                    $('figure', this).each(function(){
                      // If this box is higher than the cached highest then store it
                      if($(this).height() > highestBox) {
                        highestBox = $(this).height();
                      }
                    });
                    // Set the height of all those children to whichever was highest
                    $('figure',this).height(highestBox);
                });
            }

            // Hero banner
            $('.hero-banner__inner').slick({
                draggable: true,
                arrows: false,
                dots: false,
                fade: true,
                speed: 900,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
                touchThreshold: 100
              });

            //our clients slider
            $('.our-clients__slider').once('hjeeplBehavior').slick({
                draggable: true,
                arrows: false,
                dots: false,
                margin: 40,
                speed: 900,
                infinite: true,
                slidesToShow: 5,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                touchThreshold: 100,
                responsive: [
                    {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    },
                ]
            });

            //Latest project slider
            $('.latest-project__slider').once('hjeeplBehavior').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: false,
                infinite: true,
                autoplay: true,
                pauseOnHover:true,
                focusOnSelect: true,
                centerPadding: 0,

                responsive: [
                    {
                        breakpoint: 576,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                        }
                    },
                ]
            });

        }
    };
})(jQuery, Drupal);
