<?php

namespace Drupal\language_negotiation\Plugin\LanguageNegotiation;

use Drupal\language\LanguageNegotiationMethodBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for identifying language from the IP address.
 *
 * @LanguageNegotiation(
 *   id = Drupal\language_negotiation\Plugin\LanguageNegotiation\LanguageNegotiationRegion::METHOD_ID,
 *   weight = -1,
 *   name = @Translation("Region"),
 *   description = @Translation("Language based on visitor's region.")
 * )
 */
class LanguageNegotiationRegion extends LanguageNegotiationMethodBase {

  /**
   * The language negotiation method id.
   */
  const METHOD_ID = 'language-negotiation-region';

  /**
   * {@inheritdoc}
   */
  public function getLangcode(Request $request = NULL) {
    $langcode = 'en';

    if ($request && $this->languageManager) {
      \Drupal::service('page_cache_kill_switch')->trigger();

      $languages = $this->languageManager->getLanguages();
      $countries = $this->config->get('ip_language_negotiation.settings')->get('ip_language_negotiation_countries') ?: array();
      $current_ip = \Drupal::request()->getClientIp();
      $country_code = $this->ip_info($current_ip);
// echo "SUY ". $country_code;
      if (!empty($country_code) && $country_code == "EU") {
        $langcode = 'fr';
      }
    }
// echo " LA ". $langcode;
    return $langcode;
  }

  public function ip_info($ip = NULL, $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }

    if (filter_var($ip, FILTER_VALIDATE_IP)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
          $output = $ipdat->geoplugin_continentCode;
        }
    }
    return $output;
  }

}


