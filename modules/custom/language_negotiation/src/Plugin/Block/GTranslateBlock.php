<?php

namespace Drupal\language_negotiation\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a block with a Google translate snippet.
 *
 * @Block(
 *   id = "gtranslate_block",
 *   admin_label = @Translation("Google Translate Custom Block"),
 * )
 */
class GTranslateBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {

    // \Drupal::service('page_cache_kill_switch')->trigger();
    $current_ip = \Drupal::request()->getClientIp();
    $country_code = $this->ip_info($current_ip);
    $is_translate = FALSE;
    if (!empty($country_code) && $country_code == "AF") {
      $is_translate = TRUE;
    }

    return [
      '#cache' => [
        'contexts' => ['ip'],
      ],
      '#theme' => 'gtranslate_block',
      '#is_translate' => $is_translate,
    ];
  }

  public function ip_info($ip = NULL, $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
      $ip = $_SERVER["REMOTE_ADDR"];
      if ($deep_detect) {
        if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
          $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
          $ip = $_SERVER['HTTP_CLIENT_IP'];
      }
    }

    if (filter_var($ip, FILTER_VALIDATE_IP)) {
      $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
      if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
        $output = $ipdat->geoplugin_continentCode;
      }
    }
    return $output;
  }
}
